﻿using System;
using System.Linq;

namespace Brickwork
{
    class Program
    {
        static void Main(string[] args)
        {
            var dimensions = Console.ReadLine().Split().Select(a => int.Parse(a)).ToArray();

            var rows = dimensions[0];
            var cols = dimensions[1];

            var firstLayer = new int[rows][];

            for (int i = 0; i < rows; i++)
            {
                var row = Console.ReadLine().Split().Select(a => int.Parse(a)).ToArray();

                firstLayer[i] = row;
            }

            var secondLayer = new int[rows][];

            secondLayer = secondLayer.Select(a => new int[cols]).ToArray();

            var currentBrick = 1;

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    if (secondLayer[row][col] != 0) //Default value - unset
                    {
                        continue;
                    }

                    if (TryDirection(firstLayer, secondLayer, row, col, 0, 1, ref currentBrick)) //Right
                    {
                        continue;
                    }

                    if (TryDirection(firstLayer, secondLayer, row, col, 1, 0, ref currentBrick)) //Bottom
                    {
                        continue;
                    }

                    Console.WriteLine(-1);
                    return;
                }
            }

            Console.WriteLine();

            foreach (var row in secondLayer)
            {
                Console.WriteLine(string.Join(' ', row));
            }
        }

        private static bool TryDirection(int[][] firstLayer, int[][] secondLayer, int row, int col, int rowOffset, int colOffset,
            ref int currentBrick)
        {
            var currentNum = firstLayer[row][col];

            var isValid = IsValidCoordinate(secondLayer, row + rowOffset, col + colOffset);

            if (!isValid) { return false; }

            var neighbourValue = firstLayer[row + rowOffset][col + colOffset];

            if (currentNum != neighbourValue)
            {
                secondLayer[row][col] = currentBrick;
                secondLayer[row + rowOffset][col + colOffset] = currentBrick;

                currentBrick++;

                return true;
            }

            return false;
        }

        private static bool IsValidCoordinate(int[][] matrix, int row, int col)
        {
            bool isValid = row >= 0 && row < matrix.Length && col >= 0 && col < matrix[0].Length;

            return isValid;
        }
    }
}
